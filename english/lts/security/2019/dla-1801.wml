<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an information disclosure
vulnerability in zookeeper, a distributed co-ordination server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0201">CVE-2019-0201</a>

    <p>Users who were not authorised to read data were able to view the access control list.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.4.9-3+deb8u2.</p>

<p>We recommend that you upgrade your zookeeper packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1801.data"
# $Id: $
