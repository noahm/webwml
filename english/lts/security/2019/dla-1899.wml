<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in faad2, the Freeware Advanced
Audio Coder:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19502">CVE-2018-19502</a>

    <p>Heap buffer overflow in the function excluded_channels (libfaad/syntax.c).
    This vulnerability might allow remote attackers to cause denial of service
    via crafted MPEG AAC data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20196">CVE-2018-20196</a>

    <p>Stack buffer overflow in the function calculate_gain (libfaad/br_hfadj.c).
    This vulnerability might allow remote attackers to cause denial of service
    or any unspecified impact via crafted MPEG AAC data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20199">CVE-2018-20199</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-20360">CVE-2018-20360</a>

    <p>NULL pointer dereference in the function ifilter_bank (libfaad/filtbank.c).
    This vulnerability might allow remote attackers to cause denial of service
    via crafted MPEG AAC data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6956">CVE-2019-6956</a>

    <p>Global buffer overflow in the function ps_mix_phase (libfaad/ps_dec.c).
    This vulnerability might allow remote attackers to cause denial of service
    or any other unspecified impact via crafted MPEG AAC data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15296">CVE-2019-15296</a>

    <p>Buffer overflow in the function faad_resetbits (libfaad/bits.c). This
    vulnerability might allow remote attackers to cause denial of service via
    crafted MPEG AAC data.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.7-8+deb8u3.</p>

<p>We recommend that you upgrade your faad2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1899.data"
# $Id: $
