<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18718">CVE-2018-18718</a> - CWE-415: Double Free
<p>The product calls free() twice on the same memory address, potentially
leading to modification of unexpected memory locations.</p>

<p>There is a suspected double-free bug with
static void add_themes_from_dir() dlg-contact-sheet.c. This method
involves two successive calls of g_free(buffer) (line 354 and 373),
and is likely to cause double-free of the buffer. One possible fix
could be directly assigning the buffer to NULL after the first call
of g_free(buffer). Thanks Tianjun Wu
<a href="https://gitlab.gnome.org/GNOME/gthumb/issues/18">https://gitlab.gnome.org/GNOME/gthumb/issues/18</a></p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3:3.3.1-2.1+deb8u1</p>

<p>We recommend that you upgrade your gthumb packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1567.data"
# $Id: $
