<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was discovered in libimage-exiftool-perl, a library
and program to read and write meta information in multimedia files,
which may result in execution of arbitrary code if a malformed DjVu
file is processed.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
10.40-1+deb9u1.</p>

<p>We recommend that you upgrade your libimage-exiftool-perl packages.</p>

<p>For the detailed security status of libimage-exiftool-perl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libimage-exiftool-perl">https://security-tracker.debian.org/tracker/libimage-exiftool-perl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2663.data"
# $Id: $
