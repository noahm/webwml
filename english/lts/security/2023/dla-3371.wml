<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in unbound, a validating,
recursive, caching DNS resolver.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3204">CVE-2022-3204</a>

 <p>A vulnerability named 'Non-Responsive Delegation Attack' (NRDelegation
 Attack) has been discovered in various DNS resolving software. The
 NRDelegation Attack works by having a malicious delegation with a
 considerable number of non responsive nameservers. The attack starts by
 querying a resolver for a record that relies on those unresponsive
 nameservers. The attack can cause a resolver to spend a lot of
 time/resources resolving records under a malicious delegation point where a
 considerable number of unresponsive NS records reside. It can trigger high
 CPU usage in some resolver implementations that continually look in the
 cache for resolved NS records in that delegation. This can lead to degraded
 performance and eventually denial of service in orchestrated attacks.
 Unbound does not suffer from high CPU usage, but resources are still needed
 for resolving the malicious delegation. Unbound will keep trying to resolve
 the record until hard limits are reached. Based on the nature of the attack
 and the replies, different limits could be reached. From now on Unbound
 introduces fixes for better performance when under load, by cutting
 opportunistic queries for nameserver discovery and DNSKEY prefetching and
 limiting the number of times a delegation point can issue a cache lookup
 for missing records.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30698">CVE-2022-30698</a>

<p>and <a href="https://security-tracker.debian.org/tracker/CVE-2022-30699">CVE-2022-30699</a></p>

 <p>Unbound is vulnerable to a novel type of the <q>ghost domain names</q> attack.
 The vulnerability works by targeting an Unbound instance.
 Unbound is queried for a rogue domain name when the cached delegation
 information is about to expire. The rogue nameserver delays the response so
 that the cached delegation information is expired. Upon receiving the
 delayed answer containing the delegation information, Unbound overwrites
 the now expired entries. This action can be repeated when the delegation
 information is about to expire making the rogue delegation information
 ever-updating. From now on Unbound stores the start time for a query and
 uses that to decide if the cached delegation information can be
 overwritten.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28935">CVE-2020-28935</a>

 <p>Unbound contains a local vulnerability that would allow for a local symlink
 attack.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.9.0-2+deb10u3.</p>

<p>We recommend that you upgrade your unbound packages.</p>

<p>For the detailed security status of unbound please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/unbound">https://security-tracker.debian.org/tracker/unbound</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3371.data"
# $Id: $
