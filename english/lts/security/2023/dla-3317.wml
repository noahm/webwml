<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were discovered in snort, a flexible Network
Intrusion Detection System, which could allow an unauthenticated, remote
attacker to cause a denial of service (DoS) condition or bypass filtering
technology on an affected device and ex-filtrate data from a compromised host.</p>

<p>For Debian 10 buster, these problems have been fixed in version
2.9.20-0+deb10u1.</p>

<p>We recommend that you upgrade your snort packages.</p>

<p>For the detailed security status of snort please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/snort">https://security-tracker.debian.org/tracker/snort</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3317.data"
# $Id: $
