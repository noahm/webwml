<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a denial-of-service vulnerability in
Django, a Python-based web development framework.</p>

<p>Passing certain inputs to multipart forms could have resulted in too
many open files or memory exhaustion, and provided a potential vector for a
denial-of-service attack.</p>

<p>The number of files parts parsed is now limited via a new
DATA_UPLOAD_MAX_NUMBER_FILES setting.</p>


<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24580">CVE-2023-24580</a>

    <p>An issue was discovered in the Multipart Request Parser in Django 3.2 before 3.2.18, 4.0 before 4.0.10, and 4.1 before 4.1.7. Passing certain inputs (e.g., an excessive number of parts) to multipart forms could result in too many open files or memory exhaustion, and provided a potential vector for a denial-of-service attack.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1:1.11.29-1+deb10u7.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3329.data"
# $Id: $
