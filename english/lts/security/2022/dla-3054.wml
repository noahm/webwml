<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13755">CVE-2017-13755</a>

    <p>Opening a crafted ISO 9660 image triggers an out-of-bounds
    read in iso9660_proc_dir() in tsk/fs/iso9660_dent.c in libtskfs.a, as
    demonstrated by fls.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13756">CVE-2017-13756</a>

    <p>Opening a crafted disk image triggers infinite recursion in
    dos_load_ext_table() in tsk/vs/dos.c in libtskvs.a, as demonstrated by
    mmls.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13760">CVE-2017-13760</a>

    <p>fls hangs on a corrupt exfat image in tsk_img_read() in
    tsk/img/img_io.c in libtskimg.a.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19497">CVE-2018-19497</a>

    <p>In The Sleuth Kit (TSK) through 4.6.4, hfs_cat_traverse in
    tsk/fs/hfs.c does not properly determine when a key length is too large,
    which allows attackers to cause a denial of service (SEGV on unknown
    address with READ memory access in a tsk_getu16 call in
    hfs_dir_open_meta_cb in tsk/fs/hfs_dent.c).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10232">CVE-2020-10232</a>

    <p>Prevent a stack buffer overflow in yaffsfs_istat by
    increasing the buffer size to the size required by tsk_fs_time_to_str.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010065">CVE-2019-1010065</a>

    <p>The Sleuth Kit 4.6.0 and earlier is affected by: Integer
    Overflow. The impact is: Opening crafted disk image triggers crash in
    tsk/fs/hfs_dent.c:237. The component is: Overflow in fls tool used on HFS
    image. Bug is in tsk/fs/hfs.c file in function hfs_cat_traverse() in lines:
    952, 1062. The attack vector is: Victim must open a crafted HFS filesystem
    image.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.4.0-5+deb9u1.</p>

<p>We recommend that you upgrade your sleuthkit packages.</p>

<p>For the detailed security status of sleuthkit please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sleuthkit">https://security-tracker.debian.org/tracker/sleuthkit</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3054.data"
# $Id: $
