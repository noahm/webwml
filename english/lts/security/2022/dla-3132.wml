<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in SnakeYaml, a YAML
parser for Java, which could facilitate a denial of service attack whenever
maliciously crafted input files are processed by SnakeYaml.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1.23-1+deb10u1.</p>

<p>We recommend that you upgrade your snakeyaml packages.</p>

<p>For the detailed security status of snakeyaml please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/snakeyaml">https://security-tracker.debian.org/tracker/snakeyaml</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3132.data"
# $Id: $
