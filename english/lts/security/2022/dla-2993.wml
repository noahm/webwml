<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One security issue has been found in a compression library
libz-mingw-w64.</p>

<p>Danilo Ramos discovered that incorrect memory handling in
libz-mingw-w64's deflate handling could result in denial of service or
potentially the execution of arbitrary code if specially crafted input
is processed.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.2.11+dfsg-1+deb9u1.</p>

<p>We recommend that you upgrade your libz-mingw-w64 packages.</p>

<p>For the detailed security status of libz-mingw-w64 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libz-mingw-w64">https://security-tracker.debian.org/tracker/libz-mingw-w64</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2993.data"
# $Id: $
