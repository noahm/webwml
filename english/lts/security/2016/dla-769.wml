<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The feature-rich screenshot program shutter uses the system() call in
an unsafe way. This allows an attacker to execute arbitrary programs
via crafted directory names.</p>


<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
0.88.3-1+deb7u1.</p>

<p>For Debian 8 <q>Jessie</q>, this problem will be fixed in version
0.92-0.1+deb8u1, part of the upcoming point release</p>

<p>For Debian 9 <q>Stretch</q> and <q>Sid</q>, this problem has been fixed in
version 0.93.1-1</p>

<p>We recommend that you upgrade your shutter packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-769.data"
# $Id: $
