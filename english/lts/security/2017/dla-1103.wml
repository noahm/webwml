<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The SDP server in BlueZ is vulnerable to an information disclosure
vulnerability which allows remote attackers to obtain sensitive information
from the bluetoothd process memory. This vulnerability lies in the processing
of SDP search attribute requests.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.99-2+deb7u1.</p>

<p>We recommend that you upgrade your bluez packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1103.data"
# $Id: $
