<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the SQL plugin in cyrus-sasl2, a library
implementing the Simple Authentication and Security Layer, is prone to a
SQL injection attack. An authenticated remote attacker can take
advantage of this flaw to execute arbitrary SQL commands and for
privilege escalation.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 2.1.27+dfsg-1+deb10u2.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2.1.27+dfsg-2.1+deb11u1.</p>

<p>We recommend that you upgrade your cyrus-sasl2 packages.</p>

<p>For the detailed security status of cyrus-sasl2 please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cyrus-sasl2">\
https://security-tracker.debian.org/tracker/cyrus-sasl2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5087.data"
# $Id: $
