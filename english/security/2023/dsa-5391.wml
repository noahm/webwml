<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in libxml2, a library providing
support to read, modify and write XML and HTML files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28484">CVE-2023-28484</a>

    <p>A NULL pointer dereference flaw when parsing invalid XML schemas may
    result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29469">CVE-2023-29469</a>

    <p>It was reported that when hashing empty strings which aren't
    null-terminated, xmlDictComputeFastKey could produce inconsistent
    results, which may lead to various logic or memory errors.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.9.10+dfsg-6.7+deb11u4.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>For the detailed security status of libxml2 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxml2">https://security-tracker.debian.org/tracker/libxml2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5391.data"
# $Id: $
