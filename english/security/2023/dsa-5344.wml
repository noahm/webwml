<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Helmut Grohne discovered a flaw in Heimdal, an implementation of
Kerberos 5 that aims to be compatible with MIT Kerberos. The backports
of fixes for <a href="https://security-tracker.debian.org/tracker/CVE-2022-3437">CVE-2022-3437</a> accidentally inverted important memory
comparisons in the arcfour-hmac-md5 and rc4-hmac integrity check
handlers for gssapi, resulting in incorrect validation of message
integrity codes.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 7.7.0+dfsg-2+deb11u3.</p>

<p>We recommend that you upgrade your heimdal packages.</p>

<p>For the detailed security status of heimdal please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/heimdal">\
https://security-tracker.debian.org/tracker/heimdal</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5344.data"
# $Id: $
