<define-tag pagetitle>Debian Installer Bookworm Alpha 2 release</define-tag>
<define-tag release_date>2023-02-19</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the second alpha release of the installer for Debian 12
<q>Bookworm</q>.
</p>

<p>
Following the
<a href="https://www.debian.org/vote/2022/vote_003">2022 General Resolution about non-free firmware</a>,
many packages containing firmware files that can be requested by the
Linux kernel at runtime were moved from non-free to non-free-firmware,
making them eligible for inclusion on official media per the updated
Social Contract #5.
</p>

<p>
Starting with this release, official images include firmware packages from
main and non-free-firmware, along with metadata to configure the installed
system accordingly. Our installation guide
<a href="https://www.debian.org/releases/bookworm/amd64/ch02s02">has been updated accordingly</a>.
</p>


<h2>Improvements in this release</h2>

<ul>
  <li>apt-setup:
  <ul>
    <li>Add an apt-setup/non-free-firmware template, similar to the
    non-free and contrib ones, used by 50mirror to enable the
    non-free-firmware component: expert install only, disabled by
    default, but automatically set when non-free firmware packages are
    installed.</li>
    <li>Factorize component handling across Debian generators, and add
    support for non-free-firmware to all of them.</li>
    <li>Avoid leaving duplicate cdrom: entries in comments at the end
    of the installation (<a href="https://bugs.debian.org/1029922">#1029922</a>).</li>
  </ul>
  </li>
  <li>base-installer:
  <ul>
    <li>Ignore multi-arch qualifier (<a href="https://bugs.debian.org/1020426">#1020426</a>).</li>
  </ul>
  </li>
  <li>cdebconf:
  <ul>
    <li>text: During progression, provide user feedback at least every
    minute.</li>
    <li>newt: Align more items on top-left in braille mode.</li>
  </ul>
  </li>
  <li>debian-cd:
  <ul>
    <li>Refresh modalias information for firmware-sof-signed.</li>
    <li>Extend generated firmware metadata with component
    information.</li>
    <li>Include firmware packages based on the configured components
    (e.g. main and non-free-firmware for official builds).</li>
    <li>Bump maximal image size to 1G for netinst CD images: 650 MB is
    no longer enough, but current images are still below 700 MB.</li>
    <li>Implement a more robust installer lookup when booting from grub-efi,
    using .disk/id/$UUID instead of .disk/info (<a href="https://bugs.debian.org/1024346">#1024346</a>, <a href="https://bugs.debian.org/1024720">#1024720</a>).</li>
    <li>All amd64 installation images will now boot on amd64 machines using
    32-bit UEFI firwmare. Users no longer need the multi-arch installer
    for those machines.</li>
    <li>Stop building multi-arch images accordingly.</li>
    <li>Delete support for win32-loader, which is no longer maintained.</li>
  </ul>
  </li>
  <li>debian-installer:
  <ul>
    <li>Switch to using bookworm artwork for the splash screens
    (Emerald).</li>
    <li>Fix missing glyphs for Georgian (<a href="https://bugs.debian.org/1017435">#1017435</a>).</li>
    <li>Bump Linux kernel ABI to 6.1.0-3.</li>
  </ul>
  </li>
  <li>debootstrap:
  <ul>
    <li>Add specific handling for the usr-is-merged package.</li>
  </ul>
  </li>
  <li>espeakup:
  <ul>
    <li>Set speakup_soft direct parameter to 1 in the installed system for
    non-English language, to fix the pronunciation of symbols and
    numbers.</li>
    <li>Avoid warning about missing mbrola voices.</li>
    <li>Run espeakup in a loop, to compensate for crashes.</li>
  </ul>
  </li>
  <li>grub-installer:
  <ul>
    <li>Fix missing debconf module integration.</li>
  </ul>
  </li>
  <li>grub2:
  <ul>
    <li>Add commented-out GRUB_DISABLE_OS_PROBER section to
    /etc/default/grub to make it easier for users to turn os-prober
    back on if they want it (<a href="https://bugs.debian.org/1013797">#1013797</a>, <a href="https://bugs.debian.org/1009336">#1009336</a>).</li>
  </ul>
  </li>
  <li>hw-detect:
  <ul>
    <li>Add support for Contents-firmware indices to speed up firmware
    lookup (from requested files to available packages), keeping the
    manual lookup as a fallback.</li>
    <li>Add support for component detection, enabling
    apt-setup/$component automatically.</li>
    <li>Resolve “usb” module into the underlying module when it
    requests firmware files (e.g. rtl8192cu for a RTL8188CUS USB Wi-Fi
    dongle), to reload the right module after deploying the relevant
    firmware package.</li>
    <li>Delete support for loading udeb firmware packages (*.udeb,
    *.ude).</li>
    <li>Note: Use cases regarding loading firmware from external
    storage remain to be clarified (<a href="https://bugs.debian.org/1029543">#1029543</a>). With firmware packages
    being included on official images, this is expected to be much
    less useful than it was.</li>
    <li>Skip the historical link up/link down dance on some network
    interfaces: while it's useful to ensure modules request firmware
    files they might require, it's actively harmful if the interface
    has been configured (e.g. manually or via preseed): the link is up
    and/or it's involved in a bond.</li>
    <li>Implement support for the hw-detect/firmware-lookup=never
    setting (<a href="https://bugs.debian.org/1029848">#1029848</a>).</li>
  </ul>
  </li>
  <li>libdebian-installer:
  <ul>
    <li>Ignore multiarch qualifier suffix (<a href="https://bugs.debian.org/1020783">#1020783</a>).</li>
  </ul>
  </li>
  <li>localechooser:
  <ul>
    <li>Enable Vietnamese in non-bogl console.</li>
  </ul>
  </li>
  <li>ifupdown:
  <ul>
    <li>Fix missing /etc/network/interfaces configuration for wireless
    connections when NetworkManager isn't installed, changing its mode
    to 600 for secure connections since it ends up containing secrets
    (<a href="https://bugs.debian.org/1029352">#1029352</a>, first issue).</li>
    <li>Adjust /etc/network/interfaces configuration for wireless
    connections with both DHCP and SLAAC: only write a DHCP stanza and
    let RAs do the rest at runtime (<a href="https://bugs.debian.org/1029352">#1029352</a>, second issue).</li>
  </ul>
  </li>
  <li>preseed:
  <ul>
    <li>Add “firmware” alias for “hw-detect/firmware-lookup”
    (<a href="https://bugs.debian.org/1029848">#1029848</a>).</li>
  </ul>
  </li>
  <li>rootskel:
  <ul>
    <li>reopen-console-linux: When speakup is requested, prefer tty1
    which is the only console that speakup can read.</li>
  </ul>
  </li>
  <li>rootskel-gtk:
  <ul>
    <li>Update artwork with the Emerald theme.
    </li>
  </ul>
  </li>
  <li>user-setup:
  <ul>
    <li>Remove support for unshadowed passwords.</li>
  </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>

<ul>
  <li>debian-installer:
  <ul>
    <li>[amd64, arm64] Build netboot images for ChromeOS devices. More
    changes are required for these images to be useful.</li>
  </ul>
  </li>
  <li>grub2:
  <ul>
    <li>Add smbios to the signed grub efi images (<a href="https://bugs.debian.org/1008106">#1008106</a>).</li>
    <li>Add serial to the signed grub efi images (<a href="https://bugs.debian.org/1013962">#1013962</a>).</li>
    <li>Don't strip Xen binaries so they work again (<a href="https://bugs.debian.org/1017944">#1017944</a>).</li>
    <li>Enable EFI zboot support on arm64 (<a href="https://bugs.debian.org/1026092">#1026092</a>).</li>
    <li>Ignore some new ext2 flags to stay compatible with latest
    mke2fs defaults (<a href="https://bugs.debian.org/1030846">#1030846</a>).</li>
  </ul>
  </li>
  <li>linux:
  <ul>
    <li>udeb: Move ledtrig-audio from sound-modules to
    kernel-image</li>
    <li>udeb: Also add drivers in subdirectories of
    drivers/net/phy</li>
    <li>[arm64] Add nvmem-imx-ocotp driver to kernel-image udeb</li>
    <li>[arm64] Add imx2_wdt driver to kernel-image udeb</li>
    <li>[arm64] Add i2c-imx to i2c-modules udeb</li>
  </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>78 languages are supported in this release.</li>
  <li>Full translation for 40 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
