<define-tag pagetitle>The Debian Project mourns the loss of Robert Lemmen, Karl Ramm and Rogério Theodoro de Brito</define-tag>
<define-tag release_date>2021-08-12</define-tag>
#use wml::debian::news
# $Id$

<p>The Debian Project has lost several members of its
community in the last year.</p>

<p>
In June 2020, Robert Lemmen passed away after a serious illness.
Robert had been regularly attending the Debian Munich meetups since the
early 00s and helped with local booths.
He had been a Debian Developer since 2007. Among other contributions, 
he packaged modules for Raku (Perl6 at that time) and helped other
contributors to get involved in the Raku Team.
He also put effort into tracking down circular dependencies in Debian.
</p>

<p>
Karl Ramm passed away in June 2020, after complications due to metastatic colon cancer.
He had been a Debian Developer since 2001 and packaged several components of MIT's 
<a href="https://en.wikipedia.org/wiki/Project_Athena">Project Athena</a>.
He was passionate about technology and Debian, 
and always interested in helping others to find and promote their passions.
</p>

<p>
In April 2021, we lost Rogério Theodoro de Brito due to the COVID-19 pandemic.
Rogério enjoyed coding small tools and had been a Debian contributor for more than 15 years.
Among other projects, he contributed toward the use of Kurobox/Linkstation devices in Debian
and maintained the youtube-dl tool. He also participated and was <q>Debian contact</q> in several upstream projects.
</p>

<p>The Debian Project honors Robert, Karl and Rogerio's good work and strong dedication to Debian
and Free Software. Their contributions will not be forgotten, and the
high standards of their work will continue to serve as an inspiration to
others.</p>

<h2>About Debian</h2>
<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely free
operating system Debian.</p>

<h2>Contact Information</h2>
<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">http://www.debian.org/</a> or send mail to
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
