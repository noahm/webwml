#use wml::debian::template title="Distribuições arquivadas"
#use wml::debian::toc
#use wml::debian::translation-check translation="35754139a03a014558f50e940f9e2ce678c59dfe"

<toc-display />

<toc-add-entry name="old-archive">debian-archive</toc-add-entry>

<p>Se você precisa acessar uma das distribuições antigas do Debian, você pode
encontrá-las nos <a hrefs="http://archive.debian.org/debian/">arquivos
do Debian</a>, <tt>https://archive.debian.org/debian/</tt>.</p>

<p>As versões são guardadas por seus codinomes sob o diretório dists/.</p>
<ul>
  <li><a href="../releases/jessie/">stretch</a> é o Debian 9.0</li>
  <li><a href="../releases/jessie/">jessie</a> é o Debian 8.0</li>
  <li><a href="../releases/wheezy/">wheezy</a> é o Debian 7.0</li>
  <li><a href="../releases/squeeze/">squeeze</a> é o Debian 6.0</li>
  <li><a href="../releases/lenny/">lenny</a> é o Debian 5.0</li>
  <li><a href="../releases/etch/">etch</a> é o Debian 4.0</li>
  <li><a href="../releases/sarge/">sarge</a> é o Debian 3.1</li>
  <li><a href="../releases/woody/">woody</A> é o Debian 3.0</li>
  <li><a href="../releases/potato/">potato</A> é o Debian 2.2</li>
  <li><a href="../releases/slink/">slink</A> é o Debian 2.1</li>
  <li><a href="../releases/hamm/">hamm</A> é o Debian 2.0</li>
  <li>bo   é o Debian 1.3</li>
  <li>rex  é o Debian 1.2</li>
  <li>buzz é o Debian 1.1</li>
</UL>

<p>Temos apenas código-fonte para versões anteriores a bo, e binários e
código-fonte para bo e versões mais recentes.
Com o passar do tempo, vamos expirar os pacotes binários para versões antigas.</p>

<p>Se você está usando o APT, as entradas relevantes do sources.list são:</p>
<pre>
  deb http://archive.debian.org/debian/ hamm contrib main non-free
</pre>
<p>ou</p>
<pre>
  deb http://archive.debian.org/debian/ bo bo-unstable contrib main non-free
</pre>

<p>A seguir está uma lista dos espelhos que incluem o repositório <q>archive</q>:</p>

#include "$(ENGLISHDIR)/distrib/archive.mirrors"
<archivemirrors>

<toc-add-entry name="non-us-archive">Repositório debian-non-US</toc-add-entry>

<p>No passado, havia software empacotado para o Debian que não podia
ser distribuído nos EUA (e outros países) devido a restrições na exportação
de criptografia ou patentes de software. O Debian mantinha um repositório
especial chamado repositório <q>non-US</q>.</p>

<p>Estes pacotes foram incorporados no repositório principal (main) no
Debian 3.1 e o repositório debian-non-US foi descontinuado; está atualmente
<em>arquivado</em>, incorporado aos repositórios archive.debian.org.</p>

<p>Eles ainda estão disponíveis a partir da máquina archive.debian.org.
Os métodos de acesso disponíveis são:</p>
<blockquote><p>
<a href="https://archive.debian.org/debian-non-US/">https://archive.debian.org/debian-non-US/</a><br>
rsync://archive.debian.org/debian-non-US/  (limitado)
</p></blockquote>

<p>Para usar estes pacotes com APT, as entradas relevantes para o sources.list
são:</p>

<pre>
  deb http://archive.debian.org/debian-non-US/ woody/non-US main contrib non-free
  deb-src http://archive.debian.org/debian-non-US/ woody/non-US main contrib non-free
</pre>
