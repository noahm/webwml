#use wml::debian::template title="Atualizações para o Debian 2.0"
#use wml::debian::translation-check translation="77fca729a2ec87b4ac92922fa059fdcb368e44c6"

<p>Vários pacotes no Debian 2.0 foram atualizados desde seu lançamento em 24 de
julho para refletir aprimoramentos de segurança e outras mudanças importantes
demais para serem deixadas até o próximo lançamento principal.

<p>O Debian está comprometido em fornecer atualizações de segurança para a
versão estável (stable) o mais rápido possível, mas também precisamos de tempo
para testar completamente essas atualizações para garantir que elas atendam aos
nossos altos padrões.

<p>Aqueles(as) que desejam ajudar no processo de teste ou sentem que uma
vulnerabilidade específica é crítica demais para esperar pelo próprio
processo de teste do Debian podem buscar pacotes atualizados do repositório
<a href="http://ftp.debian.org/debian/dists/proposed-updates/">proposed-updates</a>.
Aqueles(as) que usam a nova ferramenta de pacote <em>apt</em> podem querer
adicionar a seguinte linha ao arquivo <code>/etc/apt/sources.list</code> para
facilitar atualizações fáceis para os pacotes mais recentes, alterando a URI
conforme apropriado:

<div class="centerblock">
<p>
<code>deb http://ftp1.us.debian.org/debian dists/proposed-updates/</code>
</p>
</div>

