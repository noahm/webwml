#use wml::debian::translation-check translation="7517aa3589edd1bd256a809d9a55d0713b93fc2b" maintainer="Jean-Pierre Giraud"
           <p class="center">
             <a style="margin-left: auto; margin-right: auto;" href="vote_001_results.dot">
               <img src="vote_001_results.png" alt="Affichage graphique des résultats">
               </a>
           </p>
             <p>
               Dans le graphique ci-dessus, les n&oelig;uds en rose n'ont pas
               obtenu la majorité requise, le bleu est
               le gagnant. L’octogone est utilisé pour les
               options qui n’ont pas battu l'option par défaut.
           </p>
           <ul>
<li>Option 1 "Cacher l'identité des développeurs votant à un scrutin particulier"</li>
<li>Option 2 "Cacher l'identité des développeurs votant à un scrutin particulier et permettre la vérification"</li>
<li>Option 3 "Réaffirmer les scrutins publics"</li>
<li>Option 4 "Aucun des précédents"</li>
           </ul>
            <p>
               Dans le tableau suivant, la correspondance ligne[x] colonne[y] représente le
               nombre de suffrages où le candidat&nbsp;x est classé devant le candidat&nbsp;y.
               Une <a href="https://fr.wikipedia.org/wiki/M%C3%A9thode_Schulze">explication
               plus détaillée de la matrice des gagnants</a> peut vous aider à comprendre ce
               tableau. Pour comprendre la méthode Condorcet, l'<a
               href="https://fr.wikipedia.org/wiki/M%C3%A9thode_Condorcet">entrée de
               Wikipédia</a> est assez instructive.
           </p>
           <table class="vote">
             <caption class="center"><strong>Grille des résultats</strong></caption>
	     <tr><th>&nbsp;</th><th colspan="4" class="center">Option</th></tr>
              <tr>
                   <th>&nbsp;</th>
                   <th>    1 </th>
                   <th>    2 </th>
                   <th>    3 </th>
                   <th>    4 </th>
              </tr>
                 <tr>
                   <th>Option 1  </th>
                   <td>&nbsp;</td>
                   <td>   72 </td>
                   <td>  114 </td>
                   <td>  149 </td>
                 </tr>
                 <tr>
                   <th>Option 2  </th>
                   <td>  144 </td>
                   <td>&nbsp;</td>
                   <td>  142 </td>
                   <td>  185 </td>
                 </tr>
                 <tr>
                   <th>Option 3  </th>
                   <td>  137 </td>
                   <td>  107 </td>
                   <td>&nbsp;</td>
                   <td>  163 </td>
                 </tr>
                 <tr>
                   <th>Option 4  </th>
                   <td>   94 </td>
                   <td>   61 </td>
                   <td>   68 </td>
                   <td>&nbsp;</td>
                 </tr>
               </table>
              <p>

En regardant à la ligne&nbsp;2, colonne&nbsp;1, <q>Cacher l'identité des développeurs
votant à un scrutin particulier et permettre la vérification</q><br/>
est classé devant <q>Cacher l'identité des développeurs votant à un scrutin particulier</q> sur 144&nbsp;bulletins<br/>
<br/>
En regardant à la ligne&nbsp;1, colonne&nbsp;2, <q>Cacher l'identité des développeurs
votant à un scrutin particulier</q><br/>
est classé devant <q>Cacher l'identité des développeurs votant à un scrutin particulier
et permettre la vérification</q> sur 72&nbsp;bulletins.<br/>

              <h3>Couples de défaites</h3>
              <ul>
                <li>L'option 2 bat l’option 3 par ( 142 -  107) =   35 voix.</li>
                <li>L'option 2 bat l’option 4 par ( 185 -   61) =  124 voix.</li>
                <li>L'option 3 bat l’option 4 par ( 163 -   68) =   95 voix.</li>
              </ul>
              <h3>Contenu de l'ensemble de Schwartz</h3>
              <ul>
                <li>Option 2 "Cacher l'identité des développeurs votant à un scrutin particulier
et permettre la vérification"</li>
              </ul>
              <h3>Gagnant</h3>
              <ul>
                <li>Option 2 "Cacher l'identité des développeurs votant à un scrutin particulier
et permettre la vérification"</li>
              </ul>
              <p>
               Debian utilise la méthode Condorcet pour les élections.
               De façon très simpliste, la méthode Condorcet pure
               pourrait s'expliquer ainsi&nbsp;: <br/>
               <q>Considérer tous les couples possibles de candidats.
                  Le gagnant selon Condorcet, s'il existe, est le
                  candidat qui bat chacun des autres candidats en duel
                  singulier.</q>
               Le problème est que dans des élections complexes, il pourrait y avoir des
               relations circulaires dans lesquels A bat B, B bat C
               et C bat A. La plupart des variations de la méthode Condorcet utilisent
               divers moyens pour résoudre ces cas. Veuillez lire la
               <a href="https://fr.wikipedia.org/wiki/M%C3%A9thode_Schulze">méthode Schulze</a>
               pour de plus amples informations. La variante de Debian est expliquée dans la
               <a href="$(HOME)/devel/constitution">constitution</a>,
               au paragraphe&nbsp;A.6
              </p>
