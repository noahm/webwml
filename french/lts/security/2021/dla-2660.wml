#use wml::debian::translation-check translation="ca927a5c2fefff3b4c7f18e033a2921e02422f7a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une problème de sécurité a été découvert dans libgetdata</p>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2021-20204">CVE-2021-20204</a>.</p>

<p>Un problème de corruption de mémoire de tas (utilisation de mémoire après
libération) peut être déclenché lors du traitement de bases de données dirfile
contrefaites de manière malveillante. Cela dégrade la confidentialité,
l’intégrité et la disponibilité de logiciels tiers utilisant libgetdata comme
bibliothèque.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.9.4-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libgetdata.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libgetdata, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libgetdata">\
https://security-tracker.debian.org/tracker/libgetdata</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2660.data"
# $Id: $
