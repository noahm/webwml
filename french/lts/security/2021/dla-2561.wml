#use wml::debian::translation-check translation="4663e986d0f254065939a24a8fb089351edb1801" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Mechanize est une bibliothèque Ruby au code source ouvert qui rend les
interactions automatisées faciles. Dans Mechanize, de la version 2.0.0
jusqu’à la version 2.7.7, il existait une vulnérabilité d’injection de
commande.</p>

<p>Les versions affectées de Mechanize permettaient d’injecter des commandes
du système d’exploitation en utilisant plusieurs méthodes de classes qui
utilisaient implicitement la méthode Kernel#open de Ruby.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 2.7.5-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-mechanize.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-mechanize, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-mechanize">\
https://security-tracker.debian.org/tracker/ruby-mechanize</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2561.data"
# $Id: $
