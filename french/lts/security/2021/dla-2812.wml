#use wml::debian::translation-check translation="10249dab241cafb6527cc5f2832e5dbaea4446bd" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de sécurité a été découvert dans botan1.10, une bibliothèque
cryptographique C++.</p>


<p>Un attaquant local ou par <q>cross-VM</q> peut être capable de récupérer
des bits d'exposants secrets comme ceux utilisés dans RSA, DH, etc. avec
l'aide de l'analyse de cache.
<a href="https://www.usenix.org/conference/usenixsecurity17/technical-sessions/presentation/wang-shuai">\
https://www.usenix.org/conference/usenixsecurity17/technical-sessions/presentation/wang-shuai</a></p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1.10.17-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets botan1.10.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de botan1.10, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/botan1.10">\
https://security-tracker.debian.org/tracker/botan1.10</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2812.data"
# $Id: $
