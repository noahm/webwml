#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes de plus de sécurité ont été corrigés dans la bibliothèque
multimédia libav. Il s’agit d’une suite de la publication de DLA-1611-1.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6823">CVE-2015-6823</a>

<p>La fonction allocate_buffers dans libavcodec/alac.c n’initialisait pas
certaines données de contexte, permettant à des attaquants distants de provoquer
un déni de service (non respect de la segmentation) ou éventuellement d’avoir un
impact non précisé à l’aide de données ALAC contrefaites (Apple Lossless Audio
Codec). Ce problème a été corrigé en nettoyant les pointeurs dans allocate_buffers()
de avcodec/alac.c.</p>

<p>Contrairement à ce qui est mentionné dans debian/changelog de la publication 6:11.12-1~deb8u2,
ce problème est seulement corrigé maintenant avec la publication 6:11.12-1~deb8u3.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6824">CVE-2015-6824</a>

<p>La fonction sws_init_context dans libswscale/utils.c n’initialisait pas certaines
structures de données pixbuf, permettant à des attaquants distants de provoquer
un déni de service (non respect de la segmentation) ou éventuellement d’avoir un
impact non précisé à l’aide de données vidéo contrefaites. Ces tampons pix sont
désormais nettoyés dans swscale/utils.c, ce qui corrige l’utilisation de mémoire
non initialisée.</p>

<p>Contrairement à ce qui est mentionné dans debian/changelog de la publication 6:11.12-1~deb8u2,
ce problème est seulement corrigé maintenant avec la publication 6:11.12-1~deb8u3.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 6:11.12-1~deb8u3.</p>


<p>Nous vous recommandons de mettre à jour vos paquets libav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1611-2.data"
# $Id: $
