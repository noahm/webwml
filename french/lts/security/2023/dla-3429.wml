#use wml::debian::translation-check translation="7f7146c964075cc9a4084f297ad83f818e673c27" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans Imagemagick, une suite
logicielle utilisée pour éditer et manipuler des images numériques.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20176">CVE-2021-20176</a>

<p>Une division par zéro a été découverte dans gem.c file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20241">CVE-2021-20241</a>

<p>Une division par zéro a été découverte dansjp2 coder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20243">CVE-2021-20243</a>

<p>Une division par zéro a été découverte dans dcm coder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20244">CVE-2021-20244</a>

<p>Une division par zéro a été découverte dans fx.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20245">CVE-2021-20245</a>

<p>Une division par zéro a été découverte dans webp coder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20246">CVE-2021-20246</a>

<p>Une division par zéro a été découverte dans resample.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20309">CVE-2021-20309</a>

<p>Une division par zéro a été découverte dans WaveImage.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20312">CVE-2021-20312</a>

<p>Un dépassement d'entier a été découvert dans WriteTHUMBNAILImage()
de coders/thumbnail.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20313">CVE-2021-20313</a>

<p>Une fuite potentielle de chiffrement a été découverte dans le calcul des
signatures dans TransformSignature().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39212">CVE-2021-39212</a>

<p>Un contournement de politique a été découvert pour les fichiers Postscript.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28463">CVE-2022-28463</a>

<p>Un débordement de tampon a été découvert dans le codeur cin.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32545">CVE-2022-32545</a>

<p>Un comportement indéfini (conversion en dehors des valeurs représentables
de type <q>unsigned char</q>) a été découvert dans la gestion de fichier psd.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32546">CVE-2022-32546</a>

<p>Un comportement indéfini (conversion en dehors des valeurs représentables
de type <q>unsigned char</q>) a été découvert dans la gestion de fichier pcl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32547">CVE-2022-32547</a>

<p>Un accès non aligné a été découvert dans property.c</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 8:6.9.10.23+dfsg-2.1+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de imagemagick,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/imagemagick">\
https://security-tracker.debian.org/tracker/imagemagick</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3429.data"
# $Id: $
