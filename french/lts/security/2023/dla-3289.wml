#use wml::debian::translation-check translation="94346e1c15d3e143ce47873de29cd62b534ed91a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été trouvées dans dojo, une boîte à outils modulaire
en JavaScript, qui pouvaient aboutir à une divulgation d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4051">CVE-2020-4051</a>

<p>Le greffon LinkDialog de Dijit Editor de dojo, versions 1.14.0 à 1.14.7i est
vulnérable à une attaque par script intersite (XSS).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23450">CVE-2021-23450</a>

<p>Vulnérabilité de pollution de prototype à l’aide de la fonction
<code>setObject()</code>.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.14.2+dfsg1-1+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dojo.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de dojo,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/dojo">\
https://security-tracker.debian.org/tracker/dojo</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3289.data"
# $Id: $
