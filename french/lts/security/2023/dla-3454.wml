#use wml::debian::translation-check translation="ec19c5b5dd45292d078e34ad22bab0178b6f529a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le cadriciel multimédia
FFmpeg, qui pouvaient aboutir à un déni de service ou éventuellement à
l’exécution de code arbitraire si des fichiers ou des flux mal formés étaient
traités.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 7:4.1.11-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ffmpeg.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ffmpeg,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ffmpeg">\
https://security-tracker.debian.org/tracker/ffmpeg</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3454.data"
# $Id: $
