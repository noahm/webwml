#use wml::debian::translation-check translation="687bd181ce51df7a9ff8b31b46204b9fda274f5b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans Subversion, un système de
gestion de versions.

Le projet « Common vulnérabilités et Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11782">CVE-2018-11782</a>

<p>Ace Olszowka a signalé que le processus du serveur svnserve de Subversion
peut se terminer quand une requête en lecture seule bien formée produit une
réponse particulière, menant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0203">CVE-2019-0203</a>

<p>Tomas Bortoli a signalé que le processus du serveur svnserve de Subversion
peut se terminer quand un client envoie certaines séquences de commandes du
protocole. Si le serveur est configuré avec un accès anonyme, cela pourrait
conduire à un déni de service distant non authentifié.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.8.10-6+deb8u7.</p>
<p>Nous vous recommandons de mettre à jour vos paquets subversion.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1903.data"
# $Id: $
