#use wml::debian::translation-check translation="6337f9665633a1901f1d713bb62be697370c8d56" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans vim, une
version améliorée de l'éditeur vi. Des dépassements de tampon, des lectures
hors limites et des déréférencements de pointeur NULL peuvent conduire à un
déni de service (plantage d'application) ou avoir un autre impact non
précisé.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2:8.0.0197-4+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets vim.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de vim, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/vim">\
https://security-tracker.debian.org/tracker/vim</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">\
https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2947.data"
# $Id: $
