#use wml::debian::translation-check translation="e867384068d86591de55d8ec058e28e5c9c1b5e4" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Node.js, un
environnement d'exécution JavaScript, qui pouvaient avoir pour conséquences
une corruption de mémoire, la validation de certificats non valables, une
pollution de prototype ou une injection de commande.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22930">CVE-2021-22930</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-22940">CVE-2021-22940</a></p>

<p>Une utilisation de mémoire après libération où un attaquant pouvait
exploiter la corruption de la mémoire pour modifier le comportement du
processus.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22939">CVE-2021-22939</a>

<p>Si l'API https de Node.js était utilisée incorrectement et
qu'<q>undefined</q> était passé au paramètre <q>rejectUnauthorized</q>,
aucune erreur n'était renvoyée et les connexions à des serveurs avec un
certificat expiré auraient pu être acceptées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21824">CVE-2022-21824</a>

<p>Du fait de la logique de formatage de la fonction
<q>console.table()</q>, il n'était pas sûr de permettre le passage d'une
entrée contrôlée par l'utilisateur au paramètre <q>properties</q> tout en
passant simultanément un objet simple avec au moins une propriété comme
premier paramètre qui pouvait être <q>__proto__</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32212">CVE-2022-32212</a>

<p>Une vulnérabilité d'injection de commande du système d'exploitation due
à une vérification insuffisante d'<q>IsAllowedHost</q> qui peut facilement
être contournée parce qu'<q>IsIPAddress</q> ne vérifie pas correctement si
une adresse IP n'est pas valable avant de faire des requêtes DBS,
permettant des attaques par rattachement DNS.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
10.24.0~dfsg-1~deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nodejs.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nodejs, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nodejs">\
https://security-tracker.debian.org/tracker/nodejs</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3137.data"
# $Id: $
