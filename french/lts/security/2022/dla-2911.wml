#use wml::debian::translation-check translation="fe9bb233e3d074c470d2e194f87ee511a9e3fc0b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans apng2gif, un outil
pour convertir des images APNG au format GIF animé. Une vérification
incorrecte des entrées de l'utilisateur peut avoir pour conséquences un
déni de service (plantage d'application) ou une possible exécution de code
arbitraire lors du traitement d'un fichier image mal formé.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.8-0.1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apng2gif.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apng2gif, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/apng2gif">\
https://security-tracker.debian.org/tracker/apng2gif</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2911.data"
# $Id: $
