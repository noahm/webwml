#use wml::debian::translation-check translation="0ddbf38f2779ad6f3b887a12001fbce7a31ed481" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans ConnMan, un
gestionnaire de connexion pour périphériques embarqués. Cela pouvait avoir
pour conséquences un déni de service ou l'exécution de code arbitraire.</p>

<p>Cette mise à jour corrige également un problème de compte de références
dans le correctif <a href="https://security-tracker.debian.org/tracker/CVE-2022-32293">CVE-2022-32293</a>
introduit dans DLA-3105-1.</p>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
1.36-2.1~deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets connman.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de connman, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/connman">\
https://security-tracker.debian.org/tracker/connman</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3144.data"
# $Id: $
