#use wml::debian::translation-check translation="f95c4fb5cb2a56eaf77f41eba98c0ac87fd478ec" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Flatpak, un cadriciel
de déploiement d’applications pour des applications de bureau.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43860">CVE-2021-43860</a>

<p>Ryan Gonzalez a découvert que Flatpak ne validait pas correctement si
les permissions affichées à l'utilisateur pour une application au moment de
son installation correspondaient aux permissions réelles accordées à
l'application au moment de son exécution. Des applications malveillantes
pourraient par conséquent s'accorder à elles-mêmes des permissions sans
l'accord de l'utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21682">CVE-2022-21682</a>

<p>Flatpak n'empêchait pas toujours un utilisateur malveillant de
flatpak-builder d'écrire dans le système de fichiers local.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1.10.7-0+deb11u1.</p>

<p>Veuillez noter que flatpak-builder a aussi besoin d'une mise à jour pour
des raisons de compatibilité et en est maintenant à la
version 1.0.12-1+deb11u1 dans Bullseye.</p>

<p>Nous vous recommandons de mettre à jour vos paquets flatpak et
flatpak-builder.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de flatpak, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/flatpak">\
https://security-tracker.debian.org/tracker/flatpak</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5049.data"
# $Id: $
