<define-tag pagetitle>Publication de Debian 10 <q>Buster</q></define-tag>
<define-tag release_date>2019-07-06</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="cbfcd50b343ab08499a29ae770a5528eda687986" maintainer="Jean-Pierre Giraud"


<p>Après 25 mois de développement, le projet Debian est fier d'annoncer sa
nouvelle version stable n° 10 (nom de code <q>Buster</q>), qui sera suivie
pendant les cinq prochaines années grâce à l'effort combiné de
<a href="https://security-team.debian.org/">l'équipe de sécurité de Debian</a> 
ainsi qu'à celui de <a href="https://wiki.debian.org/LTS">l'équipe de gestion
à long terme de Debian</a>.
</p>

<p>
Debian 10 <q>Buster</q> propose de nombreuses applications et environnements
de bureau. Entre autres, elle inclut maintenant les environnements de bureau
suivants :
</p>
<ul>
<li>Cinnamon 3.8 ;</li>
<li>GNOME 3.30 ;</li>
<li>KDE Plasma 5.14 ;</li>
<li>LXDE 0.99.2 ;</li>
<li>LXQt 0.14 ;</li>
<li>MATE 1.20 ;</li>
<li>Xfce 4.12.</li>
</ul>

<p>
Dans cette version, GNOME utilise par défaut le serveur d'affichage Wayland
à la place de Xorg. Wayland est de conception plus simple et plus moderne, ce
qui est avantageux du point de vue de la sécurité. Cependant, le serveur
d'affichage Xorg est toujours installé par défaut et le gestionnaire de session
par défaut offre encore le choisir Xorg comme du serveur d'affichage pour la
session à venir.
</p>

<p>
Grâce au projet des Constructions reproductibles, plus de 91 % des paquets
source fournis dans Debian 10 construiront des paquets binaires identiques au
bit près. Il s’agit d’une propriété de vérifiabilité importante protégeant les
utilisateurs des tentatives malveillantes d'altération des compilateurs et des
réseaux de construction. Les versions futures de Debian incluront des outils et
des métadonnées de manière à ce que les utilisateurs finaux puissent valider la
provenance des paquets dans l'archive.
</p>

<p>
Pour les utilisateurs dans des environnements sensibles du point de vue de la
sécurité, AppArmor, un cadre de contrôle d'accès pour limiter les possibilités
des programmes, est installé et activé par défaut.
En outre, toutes les méthodes utilisées par APT (sauf cdrom, gpgv et rsh)
peuvent de façon optionnelle utiliser le bac à sable <q>seccomp-BPF</q>.
La méthode https de APT est incluse dans le paquet apt et n'a pas besoin
d'être installée séparément.
</p>

<p>
Le filtrage réseau est basé par défaut sur l'infrastructure nftables dans
Debian 10 <q>Buster</q>. Depuis iptables v1.8.2, le paquet binaire inclut
iptables-nft et iptables-legacy, deux variantes de l'interface en
ligne de commande d'iptables. La variante basée sur nftables utilise le
sous-système nf_tables du noyau Linux. Le système <q>alternatives</q>
peut être utilisé pour choisir entre les deux variantes.
</p>

<p>
La gestion de l'UEFI (<q>Unified Extensible Firmware Interface</q>), introduite 
dans Debian 7 (nom de code <q>Wheezy</q>) continue à être largement améliorée
dans Debian 10 <q>Buster</q>. La prise en charge de « Secure Boot » 
est incluse dans cette version pour les architectures amd64, i386 et arm64
et elle devrait fonctionner sans intervention sur la plupart des machines où
Secure Boot est activé. Cela signifie que les utilisateurs ne seront plus
obligés de désactiver la prise en charge de Secure Boot dans la configuration
du microprogramme.
</p>

<p>
Les paquets cups et cups-filters sont installés par défaut dans Debian 10
<q>Buster</q> fournissant aux utilisateurs tout ce dont ils ont besoin pour
profiter de l'impression sans pilote. Les files d'attente d'impression réseau
et les imprimantes IPP seront automatiquement installées et gérées par
cups-browsed et l'utilisation de pilotes d'impression non libres de fabricants
devient superflue.
</p>

<p>
Debian 10 <q>Buster</q> inclut un nombre important de paquets logiciels mis à niveau,
(plus de 62 % des paquets de la version précédente), tels que :
</p>
<ul>
<li>Apache 2.4.38 ;</li>
<li>BIND DNS Server 9.11 ;</li>
<li>Chromium 73.0 ;</li>
<li>Emacs 26.1 ;</li>
<li>Firefox 60.7 (dans le paquet firefox-esr) ;</li>
<li>GIMP 2.10.8 ;</li>
<li>l’ensemble de compilateurs GNU, GCC 7.4 et 8.3 ;</li>
<li>GnuPG 2.2 ;</li>
<li>Golang 1.11 ;</li>
<li>Inkscape 0.92.4 ;</li>
<li>LibreOffice 6.1 ;</li>
<li>Linux séries 4.19 ;</li>
<li>MariaDB 10.3 ;</li>
<li>OpenJDK 11 ;</li>
<li>Perl 5.28 ;</li>
<li>PHP 7.3 ;</li>
<li>PostgreSQL 11 ;</li>
<li>Python 3 3.7.2 ;</li>
<li>Ruby 2.5.1 ;</li>
<li>Rustc 1.34 ;</li>
<li>Samba 4.9 ;</li>
<li>systemd 241 ;</li>
<li>Thunderbird 60.7.2 ;</li>
<li>Vim 8.1 ;</li>
<li>plus de 59 000 autres paquets prêts à l'emploi, construits à partir de près de 29 000 paquets source.</li>
</ul>

<p>
Avec cette large sélection de paquets, ainsi que sa traditionnelle large
gestion des architectures, Debian, une fois de plus, confirme son but d'être
le système d'exploitation universel. Elle est appropriée pour bien des cas
différents d'utilisation ; de systèmes de bureau aux miniportables ; des
serveurs de développement aux systèmes pour grappe, ainsi que pour des serveurs
de bases de données, des serveurs web ou des serveurs de stockage. En même
temps, des efforts additionnels d'assurance qualité tels que des tests
automatiques d'installation et de mise à niveau pour tous les paquets de
l'archive Debian garantissent que <q>Buster</q> répond aux fortes attentes
de nos utilisateurs lors d'une publication stable de Debian.
</p>

<p>
Un total de dix architectures sont gérées :
PC 64 bits/Intel EM64T/x86-64 (<code>amd64</code>),
PC 32 bits/Intel IA-32 (<code>i386</code>),
PowerPC 64 bits Motorola/IBM petit-boutiste (<code>ppc64el</code>),
IBM S/390 64 bits (<code>s390x</code>),
pour ARM <code>armel</code> et <code>armhf</code> pour les anciens et
nouveaux matériels 32 bits, plus <code>arm64</code> pour l'architecture
64 bits <q>AArch64</q>,
et pour MIPS, <code>mips</code> (gros-boutiste) et <code>mipsel</code>
(petit-boutiste) pour les matériels 32 bits et 
<code>mips64el</code> pour le matériel 64 bits petit-boutiste.
</p>

<h3>Vous voulez l'essayer ?</h3>
<p>
Si vous voulez simplement essayer Debian 10 <q>Buster</q> sans l'installer,
vous pouvez utiliser une des <a href="$(HOME)/CD/live/">images autonomes</a> « live »
qui charge et exécute le système d'exploitation complet, dans un mode en
lecture seule, dans la mémoire de votre ordinateur.
</p>
<p>
Ces images autonomes sont fournies pour les architectures <code>amd64</code> et
<code>i386</code> et sont disponibles pour des installations à l'aide de DVD,
clés USB et amorçage par le réseau. Les utilisateurs peuvent choisir entre
plusieurs environnements de bureau : Cinnamon, GNOME, KDE Plasma, LXDE, MATE, Xfce et,
nouveauté dans Buster, LXQt. Debian <q>Buster</q> autonome réintroduit l'image
autonome standard, ainsi, il est possible d'essayer un système Debian de base
sans interface utilisateur graphique.
</p>
<p>
Si le système d'exploitation vous plaît, vous avez la possibilité de
l'installer sur le disque dur de votre machine à partir de l'image autonome.
Celle-ci comprend l'installateur indépendant Calamares ainsi que l'installateur
Debian standard. Davantage d'informations sont disponibles dans les
<a href="$(HOME)/releases/buster/releasenotes">notes de publication</a>
et sur la page de la <a href="$(HOME)/CD/live/">section des images
d'installation autonomes</a> du site de Debian.
</p>

<p>
Si vous souhaitez plutôt installer Debian 10 <q>Buster</q> directement sur le
disque dur de votre ordinateur, vous pouvez choisir parmi les nombreux supports
d'installation tels que les disques Blu-ray, les CD et DVD et les clefs USB,
ainsi que par une connexion réseau. Plusieurs environnements de bureau &mdash; Cinnamon,
GNOME, KDE Plasma Desktop et Applications, LXDE, LXQt, MATE et Xfce &mdash;
peuvent être installés grâce à ces images. De plus, des CD <q>multi-architecture</q>
sont disponibles et permettent l'installation à partir d'un choix de plusieurs
architectures à partir d'un seul disque. Ou alors, vous pouvez toujours créer
un média d'installation amorçable sur clef USB (voir le
<a href="$(HOME)/releases/stretch/installmanual">guide d'installation</a>
pour davantage de détails). 
</p>

<p>
Pour les utilisateurs de nuages, Debian propose la prise en charge directe de
beaucoup des plateformes de nuage bien connues. Les images officielles de
Debian sont faciles à sélectionner sur chaque offre d'images. Debian publie
également des <a href="https://cloud.debian.org/images/openstack/current/">images
OpenStack</a> toutes prêtes pour les architectures <code>amd64</code> et
<code>arm64</code>, prêtes à être téléchargées et utilisées dans des
configurations de nuage locales.
</p>

<p>
Debian peut maintenant être installée en 76 langues, dont la plupart sont
disponibles avec des interfaces utilisateur texte et graphique.
</p>

<p>
Les images d'installation peuvent être téléchargées dès à présent au moyen de
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (le moyen recommandé),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> ou
<a href="$(HOME)/CD/http-ftp/">HTTP</a> ; consultez la page
<a href="$(HOME)/CD/">Debian sur CD</a> pour plus d'informations. <q>Buster</q>
sera également bientôt disponible sur DVD, CD et disques Blu-ray physiques
chez de nombreux <a href="$(HOME)/CD/vendors">distributeurs</a>.
</p>


<h3>Mise à niveau de Debian</h3>
<p>
La mise à niveau vers Debian 10 à partir de la version précédente, Debian 9
(nom de code <q>Stretch</q>) est gérée automatiquement par l'outil de gestion de
paquets apt pour la plupart des configurations. Comme toujours, les
systèmes Debian peuvent être mis à niveau sans douleur, sur place, et sans période
d'indisponibilité forcée, mais il est fortement recommandé de lire les
<a href="$(HOME)/releases/buster/releasenotes">notes de publication</a> ainsi
que le <a href="$(HOME)/releases/buster/installmanual">guide d'installation</a>
pour d'éventuels problèmes et pour des instructions détaillées sur
l'installation et la mise à niveau. Les notes de publications seront améliorées
et traduites dans les semaines suivant la publication. 
</p>


<h2>À propos de Debian</h2>

<p>
Debian est un système d'exploitation libre, développé par plusieurs milliers
de volontaires provenant du monde entier qui collaborent à l'aide d'Internet.
Les points forts du projet sont l'implication basée sur le volontariat,
l'engagement dans le contrat social de Debian et le logiciel libre, ainsi que
son attachement à fournir le meilleur système d'exploitation possible. Cette 
nouvelle version représente une nouvelle étape importante dans ce sens.
</p>


<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian à l'adresse <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez
un courrier électronique à &lt;press@debian.org&gt;.
</p>
